DROP TABLE IF EXISTS public."Rankings";


CREATE TABLE IF NOT EXISTS public."Rankings"
(
    id uuid NOT NULL UNIQUE,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL UNIQUE,
    avatar character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    score integer NOT NULL DEFAULT 0,
    "totalScore" integer NOT NULL DEFAULT 0,
    "sessionId" integer,
    CONSTRAINT "Rankings_pkey" PRIMARY KEY (id)
    )
    WITH (
        OIDS = FALSE
        )
    TABLESPACE pg_default;


INSERT INTO public."Rankings" (id, email, avatar, score, "totalScore", "sessionId")
VALUES  ('8d0cc366-6299-11ee-8c99-0242ac120002', 'toto@toto.fr', 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80', 100, 800, 3);


