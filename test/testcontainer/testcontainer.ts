import { PostgreSqlContainer } from '@testcontainers/postgresql';
import { FileToCopy } from 'testcontainers/build/types';
import * as path from 'path';

const file = {
  source: path.resolve(__dirname, 'init.sql'),
  target: '/docker-entrypoint-initdb.d/init.sql',
} as FileToCopy;
export function getPostgreSqlContainer() {
  return new PostgreSqlContainer('postgres')
    .withExposedPorts(5432)
    .withDatabase('ranking-db')
    .withUsername('root')
    .withPassword('root')
    .withCopyFilesToContainer([file]);
}
