import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { getPostgreSqlContainer } from './testcontainer/testcontainer';
import { databaseConfig } from '../src/db/database-config';
import { StartedPostgreSqlContainer } from '@testcontainers/postgresql';
import { JwtService } from '@nestjs/jwt';
import * as cookieParser from 'cookie-parser';

let pg: StartedPostgreSqlContainer;
const ranking = {
  id: '8d0cc366-6299-11ee-8c99-0242ac120002',
  email: 'toto@toto.fr',
  avatar:
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  score: 100,
  totalScore: 800,
  sessionId: 3,
};

describe('RankingController (e2e)', () => {
  let app: INestApplication;
  let mockJwtService: JwtService;

  beforeEach(async () => {
    pg = await getPostgreSqlContainer().start();

    databaseConfig.development.port = pg.getMappedPort(5432);
    databaseConfig.development.host = pg.getHost();

    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.use(cookieParser());

    mockJwtService = moduleFixture.get<JwtService>(JwtService);

    await app.init();
  });

  afterEach(async () => {
    await pg.stop();
  });

  it('/ranking (GET) should return Unauthorized if no token', () => {
    return request(app.getHttpServer())
      .get('/ranking')
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (GET) should return Unauthorized if token is invalid', () => {
    jest.spyOn(mockJwtService, 'verifyAsync').mockRejectedValue({});
    return request(app.getHttpServer())
      .get('/ranking')
      .set('Cookie', ['sign_in=invalid_token'])
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (GET) should rankings', () => {
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    return request(app.getHttpServer())
      .get('/ranking')
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect([ranking]);
  });

  it('/ranking (POST) should return Unauthorized if no token', () => {
    return request(app.getHttpServer())
      .post('/ranking')
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (POST) should return Unauthorized if token is invalid', () => {
    jest.spyOn(mockJwtService, 'verifyAsync').mockRejectedValue({});
    return request(app.getHttpServer())
      .post('/ranking')
      .set('Cookie', ['sign_in=invalid_token'])
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (POST) should return ranking update for current game', async () => {
    const body = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120002',
      email: 'toto@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 100,
      sessionId: 3,
    };
    const response = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120002',
      email: 'toto@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 200,
      sessionId: 3,
    };
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    await request(app.getHttpServer())
      .post('/ranking')
      .send(body)
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect(response);
    return request(app.getHttpServer())
      .get('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect({
        id: '8d0cc366-6299-11ee-8c99-0242ac120002',
        email: 'toto@toto.fr',
        avatar:
          'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        score: 200,
        totalScore: 900,
        sessionId: 3,
      });
  });

  it('/ranking (POST) should return ranking update for current game and new user', () => {
    const body = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120003',
      email: 'tutu@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 100,
      sessionId: 3,
    };
    const response = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120003',
      email: 'tutu@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 100,
      sessionId: 3,
    };
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    return request(app.getHttpServer())
      .post('/ranking')
      .send(body)
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect(response);
  });

  it('/ranking (POST) should return ranking update for new game', () => {
    const body = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120002',
      email: 'toto@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 100,
      totalScore: 800,
      sessionId: 4,
    };
    const response = {
      id: '8d0cc366-6299-11ee-8c99-0242ac120002',
      email: 'toto@toto.fr',
      avatar:
        'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      score: 100,
      sessionId: 4,
    };
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    return request(app.getHttpServer())
      .post('/ranking')
      .send(body)
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect(response);
  });

  it('/ranking (DELETE) should return Unauthorized if no token', () => {
    return request(app.getHttpServer())
      .delete('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (DELETE) should return Unauthorized if token is invalid', () => {
    jest.spyOn(mockJwtService, 'verifyAsync').mockRejectedValue({});
    return request(app.getHttpServer())
      .delete('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .set('Cookie', ['sign_in=invalid_token'])
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking (DELETE) should delete a score', () => {
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    return request(app.getHttpServer())
      .delete('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect({
        message: 'score for 8d0cc366-6299-11ee-8c99-0242ac120002 is delete',
      });
  });

  it('/ranking/uuid (GET) should return Unauthorized if no token', () => {
    return request(app.getHttpServer())
      .get('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking/uuid (GET) should return Unauthorized if token is invalid', () => {
    jest.spyOn(mockJwtService, 'verifyAsync').mockRejectedValue({});
    return request(app.getHttpServer())
      .get('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .set('Cookie', ['sign_in=invalid_token'])
      .expect(401)
      .expect({ message: 'Unauthorized', statusCode: 401 });
  });

  it('/ranking/uuid (GET) should return score for user', () => {
    jest
      .spyOn(mockJwtService, 'verifyAsync')
      .mockResolvedValue({ user: 'toto' });
    return request(app.getHttpServer())
      .get('/ranking/8d0cc366-6299-11ee-8c99-0242ac120002')
      .set('Cookie', ['sign_in=valid_token'])
      .expect(200)
      .expect({
        id: '8d0cc366-6299-11ee-8c99-0242ac120002',
        email: 'toto@toto.fr',
        avatar:
          'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        score: 100,
        totalScore: 800,
        sessionId: 3,
      });
  });
});
