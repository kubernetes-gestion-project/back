import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { RankingModule } from './ranking/ranking.module';
import { DatabaseModule } from './db/database.module';
import { ConfigModule } from '@nestjs/config';
import { AuthMiddleware } from './auth/auth.middleware';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './ranking/constants/constant';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    DatabaseModule,
    RankingModule,
    JwtModule.register({
      global: true,
      publicKey: jwtConstants.secret,
    }),
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AuthMiddleware).forRoutes('ranking');
  }
}
