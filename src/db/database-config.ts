import * as dotenv from 'dotenv';
import { DatabaseConfig } from './models/database-config.model';
import { DB_DIALECT } from '../common/constant';

dotenv.config();

export const databaseConfig: DatabaseConfig = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: DB_DIALECT,
    define: { timestamps: process.env.DB_TIMESTAMPS === 'true' },
    logging: process.env.DB_LOGGING === 'true',
  },
};
