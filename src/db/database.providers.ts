import { Sequelize } from 'sequelize-typescript';
import { databaseConfig } from './database-config';
import { DEV, SEQUELIZE } from '../common/constant';
import { Ranking } from '../ranking/entities/ranking.entity';
require('dotenv');

export const databaseProviders = [
  {
    provide: SEQUELIZE,
    useFactory: async () => {
      let config;
      switch (process.env.NODE_ENV) {
        case DEV:
          config = databaseConfig.development;
          break;
        default:
          config = databaseConfig.development;
      }
      console.log('Config: ', config)

      const sequelize = new Sequelize(config);
      sequelize.addModels([Ranking]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
