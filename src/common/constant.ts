export const SEQUELIZE = 'SEQUELIZE';
export const DEV = 'DEV';
export const PROD = 'production';
export const LOCAL = 'LOCAL';
export const TEST = 'test';
export const DB_DIALECT = 'postgres';
export const DB_TIMESTAMPS = false;
