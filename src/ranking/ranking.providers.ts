import { RANKING_REPOSITORY } from './constants/constant';
import { Ranking } from './entities/ranking.entity';

export const rankingProviders = [
  {
    provide: RANKING_REPOSITORY,
    useValue: Ranking,
  },
];
