import { Inject, Injectable } from '@nestjs/common';
import { UpdateRankingDto } from './dto/update-ranking.dto';
import { Ranking } from './entities/ranking.entity';
import { RANKING_REPOSITORY } from './constants/constant';
import { RankingDto } from './dto/ranking.dto';
import { RankingsDto } from './dto/rankings.dto';

@Injectable()
export class RankingService {
  constructor(
    @Inject(RANKING_REPOSITORY)
    private readonly rankingRepository: typeof Ranking,
  ) {}

  async findAll() {
    return (await this.rankingRepository.findAll()) as RankingsDto[];
  }

  async findOne(id: string) {
    return (await this.rankingRepository.findOne({
      where: { id: id },
    })) as RankingsDto;
  }

  async update(updateRankingDto: UpdateRankingDto) {
    let ranking = await this.rankingRepository.findByPk(updateRankingDto.id);
    if (!ranking) {
      ranking = await this.rankingRepository.create<Ranking>({
        id: updateRankingDto.id,
        email: updateRankingDto.email,
        avatar: updateRankingDto.avatar,
      } as Ranking);
    }

    await this.rankingRepository.update(
      {
        avatar: updateRankingDto.avatar,
        score:
          ranking.sessionId !== updateRankingDto.sessionId
            ? updateRankingDto.score
            : ranking.score + updateRankingDto.score,
        totalScore: ranking.totalScore + updateRankingDto.score,
        sessionId: updateRankingDto.sessionId,
      },
      { where: { id: updateRankingDto.id } },
    );
    return await ranking.reload().then((result) => {
      return {
        id: result.id,
        avatar: result.avatar,
        email: result.email,
        score: result.score,
        sessionId: result.sessionId,
      } as RankingDto;
    });
  }

  async remove(id: string) {
    return await this.rankingRepository.destroy({ where: { id: id } });
  }
}
