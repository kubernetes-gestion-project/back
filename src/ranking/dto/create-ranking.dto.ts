import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRankingDto {
  @ApiProperty({
    example: '8d0cc366-6299-11ee-8c99-0242ac120002',
    description: 'The UUID of user',
  })
  @IsNotEmpty()
  @IsUUID()
  id: string;

  @ApiProperty({
    example: 'toto@toto.fr',
    description: 'The email of user',
  })
  @IsNotEmpty()
  @IsString()
  email: string;
}
