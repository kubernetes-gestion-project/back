import { ApiProperty } from '@nestjs/swagger';

export class RankingsDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  avatar: string;

  @ApiProperty()
  score: number;

  @ApiProperty()
  totalScore: number;

  @ApiProperty()
  sessionId: number;
}
