import { ApiProperty } from '@nestjs/swagger';

export class RankingDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  avatar: string;

  @ApiProperty()
  score: number;

  @ApiProperty()
  sessionId: number;
}
