import { PartialType } from '@nestjs/mapped-types';
import { CreateRankingDto } from './create-ranking.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class UpdateRankingDto extends PartialType(CreateRankingDto) {
  @ApiProperty({
    example: '8d0cc366-6299-11ee-8c99-0242ac120002',
    description: 'The UUID of user',
  })
  @IsNotEmpty()
  @IsUUID()
  id: string;

  @ApiProperty({
    example: 'toto@toto.fr',
    description: 'The email of user',
  })
  @IsNotEmpty()
  @IsString()
  email: string;

  @ApiProperty({
    example:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    description: 'The avatar of user',
  })
  @IsNotEmpty()
  @IsString()
  avatar: string;

  @ApiProperty({
    example: 2,
    description: 'The score of user',
  })
  @IsNotEmpty()
  @IsInt()
  score: number;

  @ApiProperty({
    example: 2,
    description: 'The session id of user',
  })
  @IsNotEmpty()
  @IsInt()
  sessionId: number;
}
