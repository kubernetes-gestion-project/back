import {
  Controller,
  Get,
  Body,
  Param,
  Delete,
  Req,
  Res,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { RankingService } from './ranking.service';
import { UpdateRankingDto } from './dto/update-ranking.dto';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { RankingsDto } from './dto/rankings.dto';
import { RankingDto } from './dto/ranking.dto';

@Controller('ranking')
export class RankingController {
  constructor(private readonly rankingService: RankingService) {}

  @Get()
  @ApiOperation({ summary: 'Retrieve a list of all score' })
  @ApiResponse({ status: HttpStatus.OK, type: RankingsDto, isArray: true })
  findAll(@Req() req, @Res() res) {
    return this.rankingService
      .findAll()
      .then((rankings) => {
        return res.status(HttpStatus.OK).send(rankings);
      })
      .catch((err) => {
        return res
          .status(HttpStatus.FORBIDDEN)
          .send({ statusCode: HttpStatus.FORBIDDEN, detail: err });
      });
  }

  @Get(':uuid')
  @ApiOperation({ summary: 'Retrieve the score for a user' })
  @ApiResponse({ status: HttpStatus.OK, type: RankingsDto })
  findOne(@Param('uuid') id: string, @Req() req, @Res() res) {
    return this.rankingService
      .findOne(id)
      .then((ranking) => {
        return res.status(HttpStatus.OK).send(ranking);
      })
      .catch((err) => {
        return res
          .status(HttpStatus.FORBIDDEN)
          .send({ statusCode: HttpStatus.FORBIDDEN, detail: err });
      });
  }

  @Post()
  @ApiOperation({ summary: 'Set the actual score for a user' })
  @ApiResponse({ status: HttpStatus.OK, type: RankingDto })
  update(@Body() updateRankingDto: UpdateRankingDto, @Req() req, @Res() res) {
    return this.rankingService
      .update(updateRankingDto)
      .then((ranking) => {
        return res.status(HttpStatus.OK).send(ranking);
      })
      .catch((err) => {
        return res
          .status(HttpStatus.FORBIDDEN)
          .send({ statusCode: HttpStatus.FORBIDDEN, detail: err });
      });
  }
  @Delete(':uuid')
  @ApiOperation({ summary: 'Delete the score for a user' })
  @ApiResponse({
    status: 200,
    description: 'Score deleted.',
    schema: {
      type: 'object',
      properties: {
        message: {
          type: 'string',
          example: 'score for {uuid} is deleted',
        },
      },
    },
  })
  remove(@Param('uuid') id: string, @Req() req, @Res() res) {
    return this.rankingService
      .remove(id)
      .then(() => {
        return res
          .status(HttpStatus.OK)
          .send({ message: `score for ${id} is delete` });
      })
      .catch((err) => {
        return res
          .status(HttpStatus.FORBIDDEN)
          .send({ statusCode: HttpStatus.FORBIDDEN, detail: err });
      });
  }
}
