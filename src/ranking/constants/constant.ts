import * as dotenv from 'dotenv';
dotenv.config();
export const RANKING_REPOSITORY = 'RANKING_REPOSITORY';

export const jwtConstants = { secret: process.env.SECRET_JWT };
