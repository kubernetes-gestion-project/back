import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import * as dotenv from 'dotenv';
import * as process from 'process';
dotenv.config();

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) {}

  async use(req: any, res: any, next: () => void) {
    if (process.env.DEBUG_JTW === 'false') {
      const token = this.extractTokenFromCookie(req);
      if (!token) {
        throw new UnauthorizedException();
      }
      try {
        const payload = await this.jwtService.verifyAsync(token);
        req['user'] = payload;
      } catch (e) {
        throw new UnauthorizedException();
      }
    }
    next();
  }

  private extractTokenFromCookie(request: Request): string | undefined {
    try {
      return request.cookies['sign_in'];
    } catch {
      throw new HttpException(
        { message: 'Cookie not found', statusCode: HttpStatus.UNAUTHORIZED },
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
